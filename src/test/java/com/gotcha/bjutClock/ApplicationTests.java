package com.gotcha.bjutClock;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gotcha.bjutClock.dto.UserDTO;
import com.gotcha.bjutClock.util.PersonInfo;
import com.gotcha.bjutClock.util.SendToWechat;
import com.gotcha.bjutClock.vo.FTQQ;
import com.gotcha.bjutClock.vo.LoginUser;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ApplicationTests {
    @Resource
    LoginUser loginUser;
    @Resource
    FTQQ ftqq;

    @Test
    void contextLoads() throws JsonProcessingException {
        List<String> cookies = PersonInfo.login(loginUser);
        UserDTO oldMsg = PersonInfo.getOldMsg(cookies);
        System.out.println(oldMsg);
        PersonInfo.setMsg(oldMsg,cookies);
    }

    @Test
    void testSendMsg() throws JsonProcessingException {
        SendToWechat.setMsg("这是一次测试"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),ftqq.getSeKey());
    }

}
