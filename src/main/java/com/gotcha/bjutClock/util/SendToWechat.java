package com.gotcha.bjutClock.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gotcha.bjutClock.vo.FTQQ;
import com.gotcha.bjutClock.vo.LoginUser;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

/**
 * @time 2020/6/15
 * @auth Gotcha
 * @describe
 */
public class SendToWechat {

    private static String FTQQ_URL = "https://sc.ftqq.com/";

    public static void setMsg(String data,String key){
        String url = FTQQ_URL+key+".send?text="+data;
        RestTemplate client = new RestTemplate();
        HttpMethod method = HttpMethod.GET;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity httpEntity = new HttpEntity(headers);
        ResponseEntity<String> response = client.exchange(url,method,httpEntity,String.class);
        System.out.println(response);
    }
}
