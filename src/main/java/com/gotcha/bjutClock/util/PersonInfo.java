package com.gotcha.bjutClock.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gotcha.bjutClock.dto.MessageDTO;
import com.gotcha.bjutClock.dto.UserDTO;
import com.gotcha.bjutClock.vo.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @time 2020/6/15
 * @auth Gotcha
 * @describe
 */
public class PersonInfo {
    private static String LOGIN_URL = "https://itsapp.bjut.edu.cn/uc/wap/login/check";
    private static String OLD_MSG_URL = "https://itsapp.bjut.edu.cn/ncov/api/default/daily?xgh=0&app_id=bjut";
    private static String SET_MSG_URL = "https://itsapp.bjut.edu.cn/ncov/wap/default/save";

    @Autowired
    LoginUser loginUser;


    public static List<String> login(LoginUser loginUser) throws JsonProcessingException {
        RestTemplate client = new RestTemplate();
        MultiValueMap<String, String> headerInfo = new LinkedMultiValueMap<>();
        headerInfo.add("User-Agent","Mozilla/5.0 (Linux; Android 10;  AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/66.0.3359.126 MQQBrowser/6.2 TBS/045136 Mobile Safari/537.36 wxwork/3.0.16 MicroMessenger/7.0.1 NetType/WIFI Language/zh");
        HttpHeaders headers = new HttpHeaders(headerInfo);
        HttpMethod method = HttpMethod.POST;
        // 以表单的方式提交
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        LinkedMultiValueMap<String, String> info=new LinkedMultiValueMap<>();
        info.add("username",loginUser.getUsername());
        info.add("password",loginUser.getPassword());
        HttpEntity httpEntity = new HttpEntity(info,headers);
        ResponseEntity<String> response = client.exchange(LOGIN_URL,method,httpEntity,String.class);

        //String body = response.getBody();
        List<String> list = new ArrayList<>();
        list.add(response.getHeaders().get("Set-Cookie").get(0));
        list.add(response.getHeaders().get("Set-Cookie").get(1));
        return list;
/*      Json格式提交
        //jackson的ObjectMapper 转换对象
        ObjectMapper mapper = new ObjectMapper();
        //将某个java对象转换为JSON字符串
        String jsonStr = mapper.writeValueAsString(loginUser);
        //HttpEntity<String> requestEntity = new HttpEntity<String>(jsonStr, headers);
        //ResponseEntity<String> response = client.exchange(URL, method, requestEntity, String.class);
*/
    }

    public static UserDTO getOldMsg(List<String> cookies) throws JsonProcessingException {
        RestTemplate client = new RestTemplate();
        MultiValueMap<String, String> headerInfo = new LinkedMultiValueMap<>();
        headerInfo.add("User-Agent","Mozilla/5.0 (Linux; Android 10;  AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/66.0.3359.126 MQQBrowser/6.2 TBS/045136 Mobile Safari/537.36 wxwork/3.0.16 MicroMessenger/7.0.1 NetType/WIFI Language/zh");
        HttpHeaders headers = new HttpHeaders(headerInfo);
        HttpMethod method = HttpMethod.GET;

        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        headers.put(HttpHeaders.COOKIE,cookies);
        HttpEntity httpEntity = new HttpEntity(headers);
        ResponseEntity<String> response = client.exchange(OLD_MSG_URL,method,httpEntity,String.class);

        String body = response.getBody();
        ObjectMapper objectMapper = new ObjectMapper();
        MessageDTO message = objectMapper.readValue(body, MessageDTO.class);
        System.out.println(body);
        UserDTO userDTO = JSON.parseObject(JSON.toJSONString(message.getD()), new TypeReference<UserDTO>(){});
        return userDTO;
    }

    public static Boolean setMsg(UserDTO userDTO,List<String> cookies){
        RestTemplate client = new RestTemplate();
        MultiValueMap<String, String> headerInfo = new LinkedMultiValueMap<>();
        headerInfo.add("User-Agent","Mozilla/5.0 (Linux; Android 10;  AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/66.0.3359.126 MQQBrowser/6.2 TBS/045136 Mobile Safari/537.36 wxwork/3.0.16 MicroMessenger/7.0.1 NetType/WIFI Language/zh");
        HttpHeaders headers = new HttpHeaders(headerInfo);
        HttpMethod method = HttpMethod.POST;
        // 以表单的方式提交
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        LinkedMultiValueMap<String, String> info=new LinkedMultiValueMap<>();
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(sdf.format(date));
        info.add("date",sdf.format(date));
        info.add("realname",userDTO.getRealname());
        info.add("number",userDTO.getNumber());
        info.add("sfzx",userDTO.getSfzx());
        info.add("sfzgn","1");
        info.add("area",userDTO.getArea());
        info.add("dqjzzt",userDTO.getDqjzzt());
        info.add("tw",userDTO.getTw());
        info.add("sftjwh",userDTO.getSftjwh());
        info.add("sftjhb",userDTO.getSftjhb());
        info.add("sfcyglq",userDTO.getSfcyglq());
        info.add("sfjcwhry",userDTO.getSfjcwhry());
        info.add("sfjchbry",userDTO.getSfjchbry());
        info.add("geo_api_info",userDTO.getGeo_api_info());
        info.add("jcjgqr",userDTO.getJcjgqr());
        info.add("sfcxtz",userDTO.getSfcxtz());
        info.add("sfjcbh",userDTO.getSfjcbh());
        info.add("sfcxzysx",userDTO.getSfcxzysx());
        info.add("ismoved",userDTO.getIsmoved());
        info.add("address",userDTO.getAddress());
        info.add("province",userDTO.getProvince());
        info.add("city",userDTO.getCity());
        info.add("old_city",userDTO.getOld_city());
        info.add("geo_api_infot",userDTO.getGeo_api_infot());
        info.add("old_szdd",userDTO.getOld_szdd());
        info.add("sfyyjc",userDTO.getSfyyjc());
        info.add("app_id","bjut");

        headers.put(HttpHeaders.COOKIE,cookies);

        HttpEntity httpEntity = new HttpEntity(info,headers);
        ResponseEntity<String> response = client.exchange(SET_MSG_URL,method,httpEntity,String.class);

        String body = response.getBody();
        System.out.println(body);
        return true;
    }


}
