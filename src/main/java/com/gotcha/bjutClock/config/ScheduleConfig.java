package com.gotcha.bjutClock.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gotcha.bjutClock.dto.UserDTO;
import com.gotcha.bjutClock.util.PersonInfo;
import com.gotcha.bjutClock.util.SendToWechat;
import com.gotcha.bjutClock.vo.FTQQ;
import com.gotcha.bjutClock.vo.LoginUser;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @time 2020/6/15
 * @auth Gotcha
 * @describe
 */
@Configuration
@EnableScheduling
public class ScheduleConfig {
    @Resource
    LoginUser loginUser;
    @Resource
    FTQQ ftqq;

    /**默认是fixedDelay 上一次执行完毕时间后执行下一轮*/
    @Scheduled(cron = "25 00 00 * * *")
    public void run() throws InterruptedException, JsonProcessingException {
        List<String> cookies = PersonInfo.login(loginUser);
        UserDTO oldMsg = PersonInfo.getOldMsg(cookies);
        PersonInfo.setMsg(oldMsg,cookies);
        System.out.println("success");
        SendToWechat.setMsg("打卡成功,时间："+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),ftqq.getSeKey());
    }
}
