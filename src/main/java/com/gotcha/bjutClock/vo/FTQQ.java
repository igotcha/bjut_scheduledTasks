package com.gotcha.bjutClock.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @time 2020/6/15
 * @auth Gotcha
 * @describe
 */
@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FTQQ {
    @Value("${user.seKey}")
    private String seKey;
}
