package com.gotcha.bjutClock.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @time 2020/6/15
 * @auth Gotcha
 * @describe 自动生成的
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageDTO {
    /**
     * e : 0
     * m : 操作成功
     * d : {}
     */

    private int e;
    private String m;
    private Object d;

}
