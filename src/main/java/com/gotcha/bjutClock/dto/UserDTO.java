package com.gotcha.bjutClock.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @time 2020/6/15
 * @auth Gotcha
 * @describe 自动生成的
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private String id;
    private String uid;
    private String date;
    private String tw;
    private String sfcxtz;
    private String sfyyjc;
    private String jcjgqr;
    private String jcjg;
    private String sfjcbh;
    private String sfcxzysx;
    private String remark;
    private String address;
    private String area;
    private String province;
    private String city;
    private String geo_api_info;
    private String created;
    private String qksm;
    private String sfzx;
    private String sfjcwhry;
    private String sfcyglq;
    private String gllx;
    private String glksrq;
    private String jcbhlx;
    private String jcbhrq;
    private String sftjwh;
    private String sftjhb;
    private String fxyy;
    private String bztcyy;
    private String ismoved;
    private String fjsj;
    private String created_uid;
    private String sfjchbry;
    private String sfjcqz;
    private Object jcqzrq;
    private String jcwhryfs;
    private String jchbryfs;
    private String xjzd;
    private String szgj;
    private String sfsfbh;
    private String jhfjrq;
    private String jhfjjtgj;
    private String jhfjhbcc;
    private String jhfjsftjwh;
    private String jhfjsftjhb;
    private String szsqsfybl;
    private String sfygtjzzfj;
    private String gtjzzfjsj;
    private String sfsqhzjkk;
    private String sqhzjkkys;
    private String dqjzzt;
    private String ljrq;
    private String ljjtgj;
    private String ljhbcc;
    private String fjrq;
    private String fjjtgj;
    private String fjhbcc;
    private String jrfjjtgj;
    private String jrfjhbcc;
    private String fjyy;
    private String szsqsfty;
    private String sfxxxbb;
    private String fjqszgj;
    private String fjq_province;
    private String fjq_city;
    private String fjq_szdz;
    private String szcs;
    private String old_sfzx;
    private String old_szgj;
    private int is_daily;
    private String jcjgt;
    private String geo_api_infot;
    private String old_city;
    private String old_szdd;
    private String number;
    private String realname;

}
