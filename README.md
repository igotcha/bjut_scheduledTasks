# bjut_scheduledTasks

#### 介绍
北京工业大学体温自动上报项目
 **项目用于学习交流，仅用于各项无异常时打卡，如有身体不适等情况还请自行如实打卡** 

#### 软件架构
Springboot


#### 安装教程

1.  克隆项目至本地
2.  修改使用说明中关键信息
3.  项目直接运行，或打包成jar在服务器上运行即可

#### 使用说明

1.  修改yml-dev文件username,password后运行，启动项目后即可打卡
2.  修改yml-dev文件seKey，改成自己的key即可，在这个网站上申请[http://sc.ftqq.com](http://sc.ftqq.com/?c=code)
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/0616/002657_ffba9c5f_1820273.jpeg "搜狗截图20200616002626.jpg")，绑定之后就可以发微信了，不进行这步也没事可以直接使用打卡功能

#### 参与贡献

感谢Swenchao https://github.com/Swenchao 项目基于 https://github.com/Swenchao/bjut_clock



